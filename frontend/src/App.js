// Dependency
import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Container } from 'react-bootstrap';

// Styles
import './styles/styles.scss';

// Pages
import Details from './pages/Details';
import Index from './pages/Index';

// Components

const App = () => {
  return (
    <div className="App">
      <h1 className="text-center mb-5">API GOT Characters</h1>
      <Container>
        <BrowserRouter>
          <Switch>
            <Route exact path="/">
              <Index />
            </Route>
            <Route path="/character/:id" exact component={Details} />
            <Route path="*">
              <h1>404 Not found</h1>
            </Route>
          </Switch>
        </BrowserRouter>
      </Container>
    </div>
  );
};

export default App;
