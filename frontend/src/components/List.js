import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';
import CharacterCard from './CharacterCard';

const List = ({ dataCharacters }) => {
  const renderCards = () => {
    return dataCharacters.map((character) => {
      return (
        <Col xs={4} md={3}>
          <CharacterCard data={character} />
        </Col>
      );
    });
  };
  return (
    <>
      {dataCharacters.length ? <Row>{renderCards(dataCharacters)}</Row> : <h2>Sin resultados</h2>}
    </>
  );
};

List.defaultProps = {
  dataCharacters: [],
};

List.propTypes = {
  dataCharacters: PropTypes.shape,
};

export default List;
