import React from 'react';
import PropTypes from 'prop-types';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const CharacterCard = ({ data }) => {
  const { id, name, image, gender, house } = data;
  return (
    <Card>
      <Card.Img src={image} />
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        {gender}
        <br />
        {house}
        <br />
        <Link to={`/character/${id}`}>Ver</Link>
      </Card.Body>
    </Card>
  );
};

CharacterCard.propTypes = {
  data: PropTypes.shape.isRequired,
};

export default CharacterCard;
