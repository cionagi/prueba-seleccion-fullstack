import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Pagination } from 'react-bootstrap';

const PaginationCustom = ({ current, totalPage, handlePagination }) => {
  const [items, setItems] = useState([]);
  useEffect(() => {
    const a = [];
    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < totalPage; i++) {
      a.push(
        <Pagination.Item
          key={i}
          active={i === current}
          onClick={() => {
            handlePagination(i);
          }}
        >
          {i + 1}
        </Pagination.Item>
      );
    }
    setItems(a);
  }, [current, totalPage]);
  return <Pagination>{items}</Pagination>;
};

export default PaginationCustom;

PaginationCustom.defaultProps = {
  current: 0,
  totalPage: 0,
  handlePagination: () => {},
};

PaginationCustom.propTypes = {
  current: PropTypes.number,
  totalPage: PropTypes.number,
  handlePagination: PropTypes.func,
};
