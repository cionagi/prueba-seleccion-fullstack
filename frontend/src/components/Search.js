import React from 'react';
import { Form } from 'react-bootstrap';
import PropTypes from 'prop-types';

const Search = ({ handleSearch }) => {
  return (
    <Form.Control
      size="lg"
      type="text"
      placeholder="Buscar"
      className="mb-5"
      onChange={(e) => {
        handleSearch(e.target.value);
      }}
    />
  );
};

export default Search;

Search.defaultProps = {
  handleSearch: () => {},
};

Search.propTypes = {
  handleSearch: PropTypes.func,
};
