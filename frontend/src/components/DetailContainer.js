import React from 'react';
import PropTypes from 'prop-types';
import { Col, Row } from 'react-bootstrap';

const DetailContainer = ({ data }) => {
  const {
    titles,
    origin,
    culture,
    religion,
    allegiances,
    name,
    slug,
    image,
    gender,
    alive,
    house,
  } = data;
  return (
    <Row className="border border-light p-2 shadow-sm p-3 mb-5 bg-white rounded">
      <Col xs={12} md={6} className="text-center ">
        <img src={image} alt="img GOT" className="rounded" />
      </Col>
      <Col xs={12} md={6}>
        <h1>{name}</h1>

        <Col xs={12}>
          <Row>
            <Col xs={12}>{slug}</Col>
            <Col xs={12}>
              Gender:
              {gender}
            </Col>
            <Col xs={12}>
              alive:
              {alive ? 'Yes' : 'No'}
            </Col>
            <Col xs={12}>
              House:
              {house}
            </Col>
            <Col xs={12}>
              Origin:
              {origin}
            </Col>
            <Col xs={12}>
              Culture:
              {culture}
            </Col>
            <Col xs={12}>
              Religion:
              {religion}
            </Col>
          </Row>
        </Col>
      </Col>
      <Col xs={12} className="mb-4">
        <h2>
          Titles
          {titles.length}
        </h2>
        {titles.map((title) => {
          return <div>{title}</div>;
        })}
      </Col>
      <Col xs={12} className="mb-4">
        <h2>
          Allegiances
          {allegiances.length}
        </h2>
        {allegiances.map((allegiance) => {
          return <div>{allegiance}</div>;
        })}
      </Col>
    </Row>
  );
};

DetailContainer.defaultProps = {
  data: {},
};

DetailContainer.propTypes = {
  data: PropTypes.shape,
};

export default DetailContainer;
