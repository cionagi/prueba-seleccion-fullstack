export const END_POINT = 'localhost:8080';
export const ROUTES = {
  CHARACTERS: '/characters',
  CHARACTERS_BY_ID: '/characters/:id',
};
