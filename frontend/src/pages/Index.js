/* eslint-disable react/no-unused-state */
import React, { Component } from 'react';
import axios from 'axios';
import { Row } from 'react-bootstrap';
import List from '../components/List';
import { END_POINT, ROUTES } from '../const/routes';
import PaginationCustom from '../components/PaginationCustom';
import Search from '../components/Search';

const callApiCharacters = () => {
  const response = axios.get(` http://${END_POINT}${ROUTES.CHARACTERS}`).then((res) => {
    const { status, data } = res;
    if (status === 200) {
      return data;
    }
    return [];
  });

  return response;
};

class Index extends Component {
  state = {
    isFetching: true,
    allCharacters: [],
    allCharactersFiltered: [],
    characterList: [],
    total: 0,
    perPage: 12,
    pagination: {
      current: 0,
      total: 0,
    },
  };

  componentDidMount() {
    callApiCharacters().then((res) => {
      const { perPage, pagination } = this.state;
      const total = res.length;
      this.setState({
        isFetching: false,
        allCharacters: [...res],
        allCharactersFiltered: [...res],
        total,
        pagination: {
          ...pagination,
          total: Math.round(total / perPage),
        },
      });
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const { pagination } = this.state;
    if (prevState.pagination.total !== pagination.total) {
      this.getCharactersByPage();
    }
    if (prevState.pagination.current !== pagination.current) {
      this.getCharactersByPage();
    }
  }

  getCharactersByPage = () => {
    const {
      allCharactersFiltered,
      perPage,
      pagination: { current },
    } = this.state;

    const cloneAllCharactersFiltered = allCharactersFiltered;
    const newAllCharacters = cloneAllCharactersFiltered.slice(
      current * perPage,
      (current + 1) * perPage
    );
    this.setState({
      characterList: newAllCharacters,
    });
  };

  handlePagination = (page) => {
    this.setState({
      pagination: {
        current: page,
      },
    });
  };

  handleSearch = (value) => {
    const { allCharacters } = this.state;
    const { perPage } = this.state;

    const filteredList = allCharacters.filter((character) => {
      const { house = '', name = '' } = character;
      return (
        name.toLowerCase().includes(value.toLowerCase()) ||
        house.toLowerCase().includes(value.toLowerCase())
      );
    });

    this.setState({
      allCharactersFiltered: [...filteredList],
      total: filteredList.length,
      pagination: { current: 0, total: Math.round(filteredList.length / perPage) },
    });
  };

  render() {
    const {
      isFetching,
      characterList,
      pagination: { current, total },
    } = this.state;
    return isFetching ? (
      'Loading'
    ) : (
      <>
        <Row>
          <Search handleSearch={this.handleSearch} />
        </Row>
        <List dataCharacters={characterList} />
        <div>
          <PaginationCustom
            totalPage={total}
            current={current}
            handlePagination={this.handlePagination}
          />
        </div>
      </>
    );
  }
}

export default Index;
