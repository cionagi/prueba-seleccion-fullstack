/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import axios from 'axios';

import DetailContainer from '../components/DetailContainer';
import { END_POINT, ROUTES } from '../const/routes';

const callApiCharacters = (id) => {
  const response = axios
    .get(` http://${END_POINT}${ROUTES.CHARACTERS_BY_ID.replace(':id', id)}`)
    .then((res) => {
      const { status, data } = res;
      if (status === 200) {
        return data;
      }
      return [];
    });

  return response;
};

class Details extends Component {
  state = {
    isFetching: true,
    character: {},
  };

  componentDidMount() {
    const {
      match: {
        params: { id },
      },
    } = this.props;
    callApiCharacters(id).then((res) => {
      this.setState({
        isFetching: false,
        character: res,
      });
    });
  }

  render() {
    const { isFetching, character } = this.state;
    return isFetching ? 'Loading' : <DetailContainer data={character} />;
  }
}

export default Details;
