export const saveInRedis = (key, data) => {
  client.set(key, data);
};

export const getInRedis = async (key) => {
  return client.get(key);
};

export const checkInRedis = async (key) => {
  return await client.exists(key, (err, ok) => {
    return ok;
  });
};
