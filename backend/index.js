// Dependency
const express = require('express');
const axios = require('axios');
const asyncRedis = require('async-redis');
const client = asyncRedis.createClient();

const app = express();
const cors = require('cors')

app.use(cors())
const PORT = 8080;

 const saveInRedis = (key, data) => {
  client.set(key, data);
};

 const getInRedis = async (key) => {
  return client.get(key);
};

 const checkInRedis = async (key) => {
  return await client.exists(key, (err, ok) => {
    return ok;
  });
};


client.on('error', function (err) {
  console.log('Redis error: ' + err);
});

client.on('ready', function () {
  console.log('Redis is ready');
});

const getAllCharactersApi = () => {
  return axios.get('https://api.got.show/api/show/characters').then((res) => {
    return res.data;
  });
};

app.get('/reset', function (req, res) {
  // Clear all redis
  client.flushall();
  res.status(200).send('Reset data ok');
});

app.get('/characters', async (req, res) => {
  const hasCharacters = await checkInRedis('characters');

  let data = false;
  if (hasCharacters) {
    data = await getInRedis('characters');
    data = JSON.parse(data);
  } else {
    data = await getAllCharactersApi();
    data.map(async (character) => {
      await saveInRedis(character.id, JSON.stringify(character));
    });
    await saveInRedis('characters', JSON.stringify(data));
  }
  res.status(200).json(data);
});

app.get('/characters/:id', async (req, res) => {
  const { id } = req.params;
  const hasCharacter = await checkInRedis(id.toString());
  if (hasCharacter) {
   const data = await getInRedis(id.toString());
    res.send(data);
  } else {
    // id no esta en BD
    res.status(404).send('El id no se encuentra en nuestra base de datos.');
  }
});

app.listen(PORT, () => {
  console.log(`Server running at: http://localhost:${PORT}/`);
});
